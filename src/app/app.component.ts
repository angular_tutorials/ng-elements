import { Component, Injector } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { createCustomElement } from '@angular/elements';

import { AlertComponent } from './alert.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  content = null;

  constructor(injector: Injector, domSanitizer: DomSanitizer) {

    const AlertElement = createCustomElement(AlertComponent, {injector: injector});

    customElements.define('app-alert', AlertElement);

    setTimeout(() => {
      this.content = domSanitizer.bypassSecurityTrustHtml("<app-alert message='Rendered dynamically'></app-alert>");
    }, 4000);
  }
}
